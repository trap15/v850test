/*
	v850test -- A V850 emulator test

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef INTERACTIVE_H_
#define INTERACTIVE_H_

#include "emucpu.h"
#include "v850.h"

#ifdef ENABLE_WATCHPOINTS

extern u32 writeaddr;
extern u32 readaddr;
extern int writedone;
extern int readdone;

#endif

void parse_command(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);

#endif

