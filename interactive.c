/*
	v850test -- A V850 emulator test

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "emucpu.h"
#include "v850.h"
#include "interactive.h"

typedef struct {
	int id;
	u32 pc;
} breakpoint_t;
static breakpoint_t breakpoints[256];
static int breakpoint_cnt = 0;
static int breakpoint_id = 0;

#ifdef ENABLE_WATCHPOINTS
typedef struct {
	int id;
	int mode;
	u32 addr;
	u32 len;
} watchpoint_t;
static watchpoint_t watchpoints[256];
static int watchpoint_cnt = 0;
static int watchpoint_id = 0;

u32 writeaddr = 0;
u32 readaddr = 0;
int writedone = 0;
int readdone = 0;
#endif

extern int emuexit;

typedef struct {
	char	cmd[256];
	char	help[256];
	void	(*exec)(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
} interactive_cmd_t;

void interactive_help(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_step(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_registers(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_information(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_interrupt(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_pipeline(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_quit(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_run(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_add_break(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_del_break(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_list_break(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
#ifdef ENABLE_WATCHPOINTS
void interactive_add_watch(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_del_watch(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
void interactive_list_watch(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str);
#define INTERACTIVE_CMD_COUNT	16
#else
#define INTERACTIVE_CMD_COUNT	13
#endif
static const interactive_cmd_t interactivetbl[INTERACTIVE_CMD_COUNT] = {
	{ "",			"",							interactive_step },
	{ "help",		"This message.",					interactive_help },
	{ "?",			"This message.",					interactive_help },
	{ "step",		"Runs a single cycle.",					interactive_step },
	{ "registers",		"Displays the current register state.",			interactive_registers },
	{ "information",	"Displays information about the emulator.",		interactive_information },
	{ "interrupt",		"interrupt <num>\n\tFires NMI #num.",			interactive_interrupt },
	{ "pipeline",		"Displays the current pipeline state.",			interactive_pipeline },
	{ "run",		"Runs until a breakpoint or watchpoint.",		interactive_run },
	{ "brkadd",		"brkadd <addr>\n\tAdds a breakpoint at addr.",		interactive_add_break },
	{ "brkdel",		"brkdel <id>\n\tDeletes the breakpoint with ID #id.",	interactive_del_break },
	{ "brklist",		"Lists all breakpoints' IDs and addresses.",		interactive_list_break },
#ifdef ENABLE_WATCHPOINTS
	{ "watchadd",		"watchadd <addr> <len> [r|w]\n\tAdds a watchpoint.",	interactive_add_watch },
	{ "watchdel",		"watchdel <id>\n\tDeletes the watchpoint with ID #id.",	interactive_del_watch },
	{ "watchlist",		"Lists all watchpoints' info.",				interactive_list_watch },
#endif
	{ "quit",		"Exits the emulator.",					interactive_quit },
};

int has_hit_breakpoint(u32 pc)
{
	int i;
	for(i = 0; i < breakpoint_cnt; i++) {
		if(breakpoints[i].pc == pc)
			return breakpoints[i].id + 1;
	}
	return 0;
}

void print_breakpoint_info(int id)
{
	int i;
	if(id == 0)
		return;
	for(i = 0; i < breakpoint_cnt; i++) {
		if(breakpoints[i].id == (id - 1)) {
			fprintf(stderr, "Hit breakpoint %d at 0x%08X\n", breakpoints[i].id, breakpoints[i].pc);
			return;
		}
	}
}

#ifdef ENABLE_WATCHPOINTS
int has_hit_watchpoint(u32 addr, int mode)
{
	int i;
	int ret = 0;
	for(i = 0; i < watchpoint_cnt; i++) {
		if(INRANGE(addr, watchpoints[i].addr, watchpoints[i].len) && (watchpoints[i].mode & mode)) {
			ret = watchpoints[i].id + 1;
		}
	}
	return ret;
}

void print_watchpoint_info(int id)
{
	int i;
	if(id == 0)
		return;
	for(i = 0; i < watchpoint_cnt; i++) {
		if(watchpoints[i].id == (id - 1)) {
			fprintf(stderr, "Hit watchpoint %d at 0x%08X for 0x%08X bytes\n", watchpoints[i].id, watchpoints[i].addr, watchpoints[i].len);
			return;
		}
	}
}
#endif

u32 xtoi(char* str)
{
	u32 out;
	int i;
	int mode = 10;
	if(strncmp(str, "0x", 2) == 0) {
		mode = 16;
		str += 2;
		str[0] = tolower(str[0]);
	}
	for(i = 0, out = 0; ; i++) {
		out *= mode;
		if(mode == 10) {
			out += (str[i] - '0');
			if((str[i + 1] < '0') || (str[i + 1] > '9'))
				return out;
		}else{
			if((str[i] >= '0') && (str[i] <= '9'))
				out += (str[i] - '0');
			if((str[i] >= 'a') && (str[i] <= 'f'))
				out += (str[i] - 'a' + 10);
			str[i + 1] = tolower(str[i + 1]);
			if(((str[i + 1] < '0') || (str[i + 1] > '9')) && ((str[i + 1] < 'a') || (str[i + 1] > 'f')))
				return out;
		}
	}
}

#ifdef ENABLE_WATCHPOINTS
void interactive_list_watch(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int i;
	(void)cpu;
	(void)ctx;
	(void)str;
	fprintf(stderr, "Watchpoints:\n");
	for(i = 0; i < watchpoint_cnt; i++) {
		fprintf(stderr, "\t%s watchpoint %d at 0x%08X for 0x%08X bytes\n", 
			(watchpoints[i].mode == 1) ? "Read" : (
			(watchpoints[i].mode == 2) ? "Write" : (
			(watchpoints[i].mode == 3) ? "Read/Write" : "?")), 
			watchpoints[i].id, watchpoints[i].addr, watchpoints[i].len);
	}
}

void interactive_add_watch(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	u32 len = 1;
	int mode = 0;
	u32 addr = xtoi(str);
	(void)cpu;
	(void)ctx;
	for(; (*str != ' ') && (*str != '\n') && (*str != 0); str++);
	if(*str == ' ') {
		str++;
		len = xtoi(str);
	}
	for(; (*str != ' ') && (*str != '\n') && (*str != 0); str++);
	if(*str == ' ') {
		str++;
		if(*str == 'r')
			mode |= 1;
		else if(*str == 'w')
			mode |= 2;
		str++;
		if(*str == 'r')
			mode |= 1;
		else if(*str == 'w')
			mode |= 2;
	}else{
		mode = 3;
	}
	watchpoints[watchpoint_cnt].id = watchpoint_id++;
	watchpoints[watchpoint_cnt].addr = addr;
	watchpoints[watchpoint_cnt].len = len;
	watchpoints[watchpoint_cnt].mode = mode;
	fprintf(stderr, "Added %s watchpoint %d at 0x%08X for 0x%08X bytes\n", 
		(mode == 1) ? "Read" : (
		(mode == 2) ? "Write" : (
		(mode == 3) ? "Read/Write" : "?")), 
		watchpoints[watchpoint_cnt].id, watchpoints[watchpoint_cnt].addr, 
		watchpoints[watchpoint_cnt].len);
	watchpoint_cnt++;
}

void interactive_del_watch(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int i;
	int id = atoi(str);
	(void)cpu;
	(void)ctx;
	for(i = 0; i < watchpoint_cnt; i++) {
		if(id == watchpoints[i].id) {
			fprintf(stderr, "Deleted %s breakpoint %d at 0x%08X\n", 
				(watchpoints[i].mode == 1) ? "Read" : (
				(watchpoints[i].mode == 2) ? "Write" : (
				(watchpoints[i].mode == 3) ? "Read/Write" : "?")), 
				watchpoints[i].id, watchpoints[i].addr);
			memcpy(&(watchpoints[i]), &(watchpoints[i + 1]), sizeof(watchpoint_t) * (watchpoint_cnt - i));
			watchpoint_cnt--;
		}
	}
}
#endif

void interactive_list_break(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int i;
	(void)cpu;
	(void)ctx;
	(void)str;
	fprintf(stderr, "Breakpoints:\n");
	for(i = 0; i < breakpoint_cnt; i++) {
		fprintf(stderr, "\tBreakpoint %d at 0x%08X\n", 
			breakpoints[i].id, breakpoints[i].pc);
	}
}

void interactive_add_break(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	u32 addr = xtoi(str);
	(void)cpu;
	(void)ctx;
	breakpoints[breakpoint_cnt].id = breakpoint_id++;
	breakpoints[breakpoint_cnt].pc = addr;
	fprintf(stderr, "Added breakpoint %d at 0x%08X\n", 
		breakpoints[breakpoint_cnt].id, breakpoints[breakpoint_cnt].pc);
	breakpoint_cnt++;
}

void interactive_del_break(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int i;
	int id = atoi(str);
	(void)cpu;
	(void)ctx;
	for(i = 0; i < breakpoint_cnt; i++) {
		if(id == breakpoints[i].id) {
			fprintf(stderr, "Deleted breakpoint %d at 0x%08X\n", 
				breakpoints[i].id, breakpoints[i].pc);
			memcpy(&(breakpoints[i]), &(breakpoints[i + 1]), sizeof(breakpoint_t) * (breakpoint_cnt - i));
			breakpoint_cnt--;
		}
	}
}

void interactive_step(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	char inst[256];
	u32 opc = ctx->pc;
	(void)str;
	EMUCPU_DASM_RUN( v850, cpu, inst, ctx->pc);
	EMUCPU_EMULATE_RUN( v850, cpu, 1);
	if(ctx->pc == opc)
		fprintf(stderr, "0x%08X\tNo fetch\n", opc);
	else
		fprintf(stderr, "0x%08X\t%s\n", opc, inst);
	if((opc = has_hit_breakpoint(ctx->pc)))
		print_breakpoint_info(opc);
#ifdef ENABLE_WATCHPOINTS
	if(readdone && (opc = has_hit_watchpoint(readaddr, 1))) {
		print_watchpoint_info(opc);
		readdone = 0;
	}
	if(writedone && (opc = has_hit_watchpoint(writeaddr, 2))) {
		print_watchpoint_info(opc);
		writedone = 0;
	}
#endif
}

void interactive_run(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	char inst[256];
	int bp;
	int wp;
	int i = 0;
	(void)str;
	for(bp = 0, wp = 0; (bp == 0) && (wp == 0); ) {
		EMUCPU_EMULATE_RUN( v850, cpu, 1);
		if((bp = has_hit_breakpoint(ctx->pc)))
			print_breakpoint_info(bp);
#ifdef ENABLE_WATCHPOINTS
		if(readdone && (wp = has_hit_watchpoint(readaddr, 1))) {
			print_watchpoint_info(wp);
			readdone = 0;
		}
		if(writedone && (wp = has_hit_watchpoint(writeaddr, 2))) {
			print_watchpoint_info(wp);
			writedone = 0;
		}
#endif
	}
#ifdef ENABLE_WATCHPOINTS
	if(wp) {
		for(i = 0; i <= V850_PIPELINE_LENGTH; i++) {
			if(ctx->pipelining[i]->laststage == V850_PIPELINE_MEM)
				break;
		}
	}
#endif
	if(bp) {
		for(i = 0; i <= V850_PIPELINE_LENGTH; i++) {
			if(ctx->pipelining[i]->laststage == V850_PIPELINE_FETCH)
				break;
		}
	}
#ifdef ENABLE_WATCHPOINTS
	if(bp || wp) {
#else
	if(bp) {
#endif
		if(i == (V850_PIPELINE_LENGTH + 1))
			i = 0;
		EMUCPU_DASM_RUN( v850, cpu, inst, ctx->pipelining[i]->pc);
		fprintf(stderr, "0x%08X\t%s\n", ctx->pipelining[i]->pc, inst);
	}
}

void interactive_registers(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int i, regs;
	cpu_info_t info;
	(void)ctx;
	(void)str;
	info.ptr = malloc(256);
	EMUCPU_GET_INFO_RUN( v850, cpu, EMUCPU_INFO_TYPE_REG_COUNT, &info);
	regs = info.i;
	for(i = 0; i < regs; i++) {
		EMUCPU_GET_INFO_RUN( v850, cpu, EMUCPU_INFO_TYPE_REG_NAME  + i, &info);
		EMUCPU_GET_INFO_RUN( v850, cpu, EMUCPU_INFO_TYPE_REG_VALUE + i, &info);
		fprintf(stderr, "%s: 0x%08X ", (char*)info.ptr, (u32)info.i);
		if((i % 4) == 3)
			fprintf(stderr, "\n");
	}
	fprintf(stderr, "\n");
	free(info.ptr);
}

void interactive_information(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	cpu_info_t info;
	(void)ctx;
	(void)str;
	info.ptr = malloc(256);
	EMUCPU_GET_INFO_RUN( v850, cpu, EMUCPU_INFO_TYPE_NAME, &info);
	fprintf(stderr, "%s: part of the ", (char*)info.ptr);
	EMUCPU_GET_INFO_RUN( v850, cpu, EMUCPU_INFO_TYPE_FAMILY, &info);
	fprintf(stderr, "%s family.\n", (char*)info.ptr);
	EMUCPU_GET_INFO_RUN( v850, cpu, EMUCPU_INFO_TYPE_VERSION, &info);
	fprintf(stderr, "Emulation core version %s. Credits: ", (char*)info.ptr);
	EMUCPU_GET_INFO_RUN( v850, cpu, EMUCPU_INFO_TYPE_CREDITS, &info);
	fprintf(stderr, "%s\n", (char*)info.ptr);
	free(info.ptr);
}

void interactive_interrupt(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int nmin = 0;
	(void)ctx;
	nmin = atoi(str);
	printf("Firing NMI%d\n", nmin);
	EMUCPU_INTERRUPT_RUN( v850, cpu, nmin | (2LL << 56));
}

char* stagestrs[V850_PIPELINE_ST_COUNT+1] = {
	"Fetch",
	"Alignment Hazard",
	"Decode",
	"Execute",
	"Memory Access",
	"Writeback",
	"Instruction done",
};

void interactive_pipeline(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	char inst[256];
	int i;
	_v850pipeline_t** pipes = ctx->pipelining;
	(void)str;
	fprintf(stderr, "Pipeline contents:\n");
	for(i = 0; i < V850_PIPELINE_LENGTH; i++) {
		EMUCPU_DASM_RUN( v850, cpu, inst, pipes[i]->pc);
		fprintf(stderr, "Pipe %d:	Stage: %s (%d) [Progress: %d]\n", i, stagestrs[pipes[i]->stage], pipes[i]->stage, pipes[i]->stageprog);
		if(pipes[i]->stage != V850_PIPELINE_DONE) {
			fprintf(stderr, "0x%08X\t%s\n", pipes[i]->pc, inst);
			fprintf(stderr, "\tOpcodes:\n"
					"\t\t0: 0x%04X\n"
					"\t\t1: 0x%04X\n",
					pipes[i]->opcode, pipes[i]->opcode2);
			fprintf(stderr, "\tOperands:\n"
					"\t\t0: 0x%08X\n"
					"\t\t1: 0x%08X\n"
					"\t\t2: 0x%08X\n"
					"\t\t3: 0x%08X\n",
					pipes[i]->oper[0], pipes[i]->oper[1], pipes[i]->oper[2], pipes[i]->oper[3]);
		}
	}
	fprintf(stderr, "Pipeline queue contents:\n");
	for(i = 0; i < V850_PIPELINE_ST_COUNT; i++) {
		fprintf(stderr, "Queue %d:	%p\n", i, ctx->pipequeue[i]);
	}
}

void interactive_quit(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	(void)cpu;
	(void)ctx;
	(void)str;
	emuexit = 1;
}

void interactive_help(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int i;
	char* ch;
	int len;
	int hits = 0;
	(void)cpu;
	(void)ctx;
	for(ch = str; (*ch != '\n') && (*ch != ' ') && (*ch != 0); ch++);
	len = ch - str;
	if(*ch == ' ')
		ch++;
	for(i = 1; i < INTERACTIVE_CMD_COUNT; i++) {
		if(strncmp(interactivetbl[i].cmd, str, len) == 0)
			hits++;
	}
	if((hits > 1) || (hits == 0)) {
		fprintf(stderr, "The following commands are available:\n");
		for(i = 3; i < INTERACTIVE_CMD_COUNT; i++) {
			fprintf(stderr, "\t%s\n", interactivetbl[i].cmd);
		}
	}else{
		for(i = 1; i < INTERACTIVE_CMD_COUNT; i++) {
			if(strncmp(interactivetbl[i].cmd, str, len) == 0)
				fprintf(stderr, "%s\n", interactivetbl[i].help);
		}
	}

}

void parse_command(cpu_ctx_t* cpu, v850ctx_t* ctx, char* str)
{
	int i;
	char* ch;
	int len;
	int hits = 0;
	for(ch = str; (*ch != '\n') && (*ch != ' ') && (*ch != 0); ch++);
	len = ch - str;
	if(*ch == ' ')
		ch++;
	if(*str == '\n') {
		interactive_step(cpu, ctx, ch);
		return;
	}
	for(i = 0; i < INTERACTIVE_CMD_COUNT; i++) {
		if(strncmp(interactivetbl[i].cmd, str, len) == 0)
			hits++;
	}
	if(hits > 1) {
		fprintf(stderr, "Ambiguous command.\n");
	}else if(hits == 0) {
		fprintf(stderr, "No matches %s %d.\n", str, len);
	}else{
		for(i = 0; i < INTERACTIVE_CMD_COUNT; i++) {
			if(strncmp(interactivetbl[i].cmd, str, len) == 0)
				interactivetbl[i].exec(cpu, ctx, ch);
		}
	}
}

