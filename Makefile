CC        = gcc
OBJECTS   = main.o interactive.o
OUTPUT    = v850test
INCLUDE   = -I./ -I../libemucpu/include/
LIBS      = -lemu_v850 -lm
CFLAGS    = -Wall -Wno-type-limits -Wextra -O3 -pedantic $(INCLUDE) -g
#CFLAGS   += -DENABLE_WATCHPOINTS
LDFLAGS   = -L../libemucpu $(LIBS) -g

V850AS      = v850-elf-as
V850CC      = v850-elf-gcc
V850LD      = v850-elf-ld
V850OBJCOPY = v850-elf-objcopy
V850ASFLAGS = 
V850CFLAGS  = -O1 -nostdlib -nostartfiles -nodefaultlibs
V850LDFLAGS = -nostdlib -s
V850ASOBJS  = asmtest.v850.o
V850COBJS   = crt0.v850.o ctest.v850.o

.PHONY: all install clean cleantest tests

all: $(OUTPUT)
%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
%.v850.o: %.s
	$(V850AS) $(V850ASFLAGS) $< -o $@
%.v850.o: %.c
	$(V850CC) $(V850CFLAGS) $< -c -o $@
$(OUTPUT): $(OBJECTS)
	$(CC) $(OBJECTS) $(LDFLAGS) -o $@
clean:
	$(RM) $(OUTPUT) $(OBJECTS)

tests: asmtest.bin ctest.bin

asmtest.bin: $(V850ASOBJS)
	$(V850LD) $(V850LDFLAGS) -e 0 $(V850ASOBJS) -o $@.elf
	$(V850OBJCOPY) -O binary $@.elf $@
ctest.bin: $(V850COBJS)
	$(V850LD) $(V850LDFLAGS) -Tv850.ld $(V850COBJS) -o $@.elf
	$(V850OBJCOPY) -O binary $@.elf $@

cleantests:
	$(RM) ctest.bin asmtest.bin ctest.bin.elf asmtest.bin.elf $(V850COBJS) $(V850ASOBJS)

