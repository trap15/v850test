/*
	v850test.s -- A simple V850 code test written in C

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;
	signed int i;
	unsigned int l;
	for(i = 0; i < 8; i++) if(i > 4) asm("nop\n"); else asm("nop\nnop\n");
	for(l = 0; l < 8; l++) if(l > 4) asm("nop\n"); else asm("nop\nnop\n");
	return 0;
}

/*
cmp X, Y
compares Y op X
*/

