/*
	v850test -- A V850 emulator test

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/time.h>

#include "emucpu.h"
#include "v850.h"
#include "interactive.h"

#define V850CLOCK		32*1000*1000
#define DEBUG			0

#define INSN_LEN		(0x00010000)
#define RAM_LEN			(0x00100000)

u8 *insn;
u8 *ram;

int emuexit = 0;

EMUCPU_READ(v850, 32, 32)
{
	u8* mem = NULL;
#ifdef ENABLE_WATCHPOINTS
	readaddr = addr;
	readdone = 1;
#endif
	if(INRANGE(addr, 0x00000000, INSN_LEN)) {
		mem = insn;
	}else if(INRANGE(addr, 0x01000000, RAM_LEN)) {
		mem = ram;
		addr -= 0x01000000;
	}else
		return 0;
	return ((mem[addr+3] << 24) | (mem[addr+2] << 16) |
		(mem[addr+1] <<  8) | (mem[addr+0] <<  0)) & mask;
}

EMUCPU_WRITE(v850, 32, 32)
{
	u32 tmp;
	u8* mem = NULL;
#ifdef ENABLE_WATCHPOINTS
	writeaddr = addr;
	writedone = 1;
#endif
	if(INRANGE(addr, 0x00000000, INSN_LEN)) {
		mem = insn;
	}else if(INRANGE(addr, 0x01000000, RAM_LEN)) {
		mem = ram;
		addr &= RAM_LEN-1;
	}else
		return;
	tmp =  ((mem[addr+0] <<  0) | (mem[addr+1] <<  8) |
		(mem[addr+2] << 16) | (mem[addr+3] << 24)) & ~mask;
	tmp |= val & mask;
	mem[addr+0] |= (tmp >>  0) & 0xFF;
	mem[addr+1] |= (tmp >>  8) & 0xFF;
	mem[addr+2] |= (tmp >> 16) & 0xFF;
	mem[addr+3] |= (tmp >> 24) & 0xFF;
}

void load_binary(char *file)
{
	size_t sz;
	FILE* fp = fopen(file, "rb");
	if(fp == NULL) {
		perror("Can't open binary");
		exit(1);
	}
	fseek(fp, 0, SEEK_END);
	sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	if(sz > INSN_LEN)
		sz = INSN_LEN;
	if(fread(insn, sz, 1, fp) != 1) {
		perror("Can't read binary");
		exit(1);
	}
	fclose(fp);
}

int main(int argc, char *argv[])
{
	cpu_ctx_t* cpu;
	v850ctx_t* ctx;
	char inst[256];
	int i;
	void* werk;
	int cycles;
	float speed, speedolds[16] = {0,};
	int inter;
	struct timeval old;
	struct timeval new;
	u32 ousec, nusec;
	if(argc < 3) {
		fprintf(stderr,
			"Bad args.\n"
			"	%s [i|f] code.bin\n", argv[0]);
		return EXIT_FAILURE;
	}
	if(argv[1][0] == 'i')
		inter = 1;
	else
		inter = 0;

	insn = malloc(INSN_LEN);
	ram = malloc(RAM_LEN);
	load_binary(argv[2]);

	cpu = EMUCPU_INIT_RUN( v850 );
	ctx = cpu->data;
	ctx->EMUCPU_READ_NAME(read32) = EMUCPU_READ_NAME(v850);
	ctx->EMUCPU_WRITE_NAME(write32) = EMUCPU_WRITE_NAME(v850);
	EMUCPU_RESET_RUN( v850 , cpu);
	if(inter) {
		for(; !emuexit;) {
			fprintf(stdout, "> ");
			werk = fgets(inst, 256, stdin);
			if(werk == NULL)
				emuexit = 1;
			else
				parse_command(cpu, ctx, inst);
		}
	}else{
		for(cycles = 0, speed = 0.0f; !emuexit;) {
			printf("\x1b[2K\x1b[0G Current speed: %f%%", speed);
			fflush(stdout);
			cycles += V850CLOCK / 100;
			gettimeofday(&old, NULL);
			ousec = old.tv_usec;
			cycles -= EMUCPU_EMULATE_RUN( v850, cpu, cycles);
			gettimeofday(&new, NULL);
			nusec = new.tv_usec;
			for(i = 0; i < 15; i++)
				speedolds[i + 1] = speedolds[i];
			speedolds[0] = 1.0f / ((float)(nusec - ousec) / 1000000.0f);
			speed = 0;
			for(i = 0; i < 16; i++)
				speed += speedolds[i];
			speed /= 16;
		}
	}
	free(insn);
	free(ram);
	return EXIT_SUCCESS;
}

