#!/bin/sh

# Copyright (C) 2007 Segher Boessenkool <segher@kernel.crashing.org>
# Copyright (C) 2009 Hector Martin "marcan" <hector@marcansoft.com>
# Copyright (C) 2009 Andre Heider "dhewg" <dhewg@V850brew.org>
# Copyright (C) 2010 Alex Marshall "trap15" <trap15@raidenii.net>

# Released under the terms of the GNU GPL, version 2

GNU_FTP="http://ftp.gnu.org/gnu"
#GNU_FTP="ftp://mirrors.usc.edu/pub/gnu"

BINUTILS_VER=2.21
BINUTILS_DIR="binutils-$BINUTILS_VER"
BINUTILS_TARBALL="binutils-$BINUTILS_VER.tar.bz2"
BINUTILS_URI="$GNU_FTP/binutils/$BINUTILS_TARBALL"

GMP_VER=5.0.2
GMP_DIR="gmp-$GMP_VER"
GMP_TARBALL="gmp-$GMP_VER.tar.bz2"
GMP_URI="$GNU_FTP/gmp/$GMP_TARBALL"

MPFR_VER=3.0.0
MPFR_DIR="mpfr-$MPFR_VER"
MPFR_TARBALL="mpfr-$MPFR_VER.tar.bz2"
#MPFR_URI="http://www.mpfr.org/mpfr-$MPFR_VER/$MPFR_TARBALL"
MPFR_URI="ftp://mirrors.usc.edu/pub/gnu/mpfr/$MPFR_TARBALL"

GCC_VER=4.5.3
GCC_DIR="gcc-$GCC_VER"
GCC_CORE_TARBALL="gcc-core-$GCC_VER.tar.bz2"
GCC_CORE_URI="$GNU_FTP/gcc/gcc-$GCC_VER/$GCC_CORE_TARBALL"

GDB_VER=7.1
GDB_DIR="gdb-$GDB_VER"
GDB_TARBALL="gdb-$GDB_VER.tar.bz2"
GDB_URI="$GNU_FTP/gdb/$GDB_TARBALL"

BUILDTYPE=$1

V850_TARGET=v850-elf

MAKEOPTS=-j9

# End of configuration section.

case `uname -s` in
	*BSD*)
		MAKE=gmake
		;;
	*)
		MAKE=make
esac

export PATH=$V850DEV/bin:$PATH

die() {
	echo $@
	exit 1
}

cleansrc() {
	[ -e $V850DEV/$BINUTILS_DIR ] && rm -rf $V850DEV/$BINUTILS_DIR
	[ -e $V850DEV/$GCC_DIR ] && rm -rf $V850DEV/$GCC_DIR
	[ -e $V850DEV/$GDB_DIR ] && rm -rf $V850DEV/$GDB_DIR
}

cleanbuild() {
	[ -e $V850DEV/build_binutils ] && rm -rf $V850DEV/build_binutils
	[ -e $V850DEV/build_gcc ] && rm -rf $V850DEV/build_gcc
	[ -e $V850DEV/build_gdb ] && rm -rf $V850DEV/build_gdb
}

download() {
	DL=1
	if [ -f "$V850DEV/$2" ]; then
		echo "Testing $2..."
		tar tjf "$V850DEV/$2" >/dev/null && DL=0
	fi

	if [ $DL -eq 1 ]; then
		echo "Downloading $2..."
		wget "$1" -c -O "$V850DEV/$2" || die "Could not download $2"
	fi
}

download2() {
	echo "Downloading $3..."
	wget "$DSPTOOL_URI/$1/$3" -c -O "$V850DEV/build_dsp/$2/$3" || die "Could not download $3"
}

extract() {
	echo "Extracting $1..."
	tar xjf "$V850DEV/$1" -C "$2" || die "Error unpacking $1"
}

makedirs() {
	mkdir -p $V850DEV/build_binutils || die "Error making binutils build directory $V850DEV/build_binutils"
	mkdir -p $V850DEV/build_gcc || die "Error making gcc build directory $V850DEV/build_gcc"
	mkdir -p $V850DEV/build_gdb || die "Error making gdb build directory $V850DEV/build_gdb"
}

buildbinutils() {
	TARGET=$1
	(
		cd $V850DEV/build_binutils && \
		$V850DEV/$BINUTILS_DIR/configure --target=$TARGET \
			--prefix=$V850DEV --disable-werror --disable-multilib && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building binutils for target $TARGET"
}

buildgcc() {
	TARGET=$1
	(
		cd $V850DEV/build_gcc && \
		$V850DEV/$GCC_DIR/configure --target=$TARGET --enable-targets=all \
			--prefix=$V850DEV --disable-multilib \
			--enable-languages=c --without-headers \
			--disable-nls --disable-threads --disable-shared \
			--disable-libmudflap --disable-libssp --disable-libgomp \
			--disable-decimal-float && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building binutils for target $TARGET"
}

buildgdb() {
	TARGET=$1
	(
		cd $V850DEV/build_gdb && \
		$V850DEV/$GDB_DIR/configure --target=$TARGET \
			--prefix=$V850DEV --disable-werror --disable-multilib \
			--disable-sim && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building gdb for target $TARGET"
}


buildv850() {
	cleanbuild
	makedirs
	echo "******* Building V850 binutils"
	buildbinutils $V850_TARGET
	echo "******* Building V850 GCC"
	buildgcc $V850_TARGET
	echo "******* Building V850 GDB"
	buildgdb $V850_TARGET
	echo "******* V850 toolchain built and installed"
}

if [ -z "$V850DEV" ]; then
	die "Please set V850DEV in your environment."
fi

case $BUILDTYPE in
	v850|clean)	;;
	"")
		die "Please specify build type (v850/clean)"
		;;
	*)
		die "Unknown build type $BUILDTYPE"
		;;
esac

if [ "$BUILDTYPE" = "clean" ]; then
	cleanbuild
	cleansrc
	exit 0
fi

download "$BINUTILS_URI" "$BINUTILS_TARBALL"
download "$GMP_URI" "$GMP_TARBALL"
download "$MPFR_URI" "$MPFR_TARBALL"
download "$GCC_CORE_URI" "$GCC_CORE_TARBALL"
download "$GDB_URI" "$GDB_TARBALL"

cleansrc

extract "$BINUTILS_TARBALL" "$V850DEV"
extract "$GCC_CORE_TARBALL" "$V850DEV"
extract "$GDB_TARBALL" "$V850DEV"
extract "$GMP_TARBALL" "$V850DEV/$GCC_DIR"
mv "$V850DEV/$GCC_DIR/$GMP_DIR" "$V850DEV/$GCC_DIR/gmp" || die "Error renaming $GMP_DIR -> gmp"
extract "$MPFR_TARBALL" "$V850DEV/$GCC_DIR"
mv "$V850DEV/$GCC_DIR/$MPFR_DIR" "$V850DEV/$GCC_DIR/mpfr" || die "Error renaming $MPFR_DIR -> mpfr"

case $BUILDTYPE in
	v850)		buildv850 ;;
esac

