/*
        crt0.s -- A simple V850 crt0

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

.extern	_main
.globl	_start

	.text
	.section .text,"ax",@progbits

.set	trap_data,		0x01000000

	/* entry */
	.org 0x00
_start:
	jr	start
	/* nmi0 */
	.org 0x10
	jr	nmi0_handler
	/* nmi1 */
	.org 0x20
	jr	nmi1_handler
	/* nmi2 */
	.org 0x30
	jr	nmi2_handler
	/* traps 0n */
	.org 0x40
	jr	trap_handler
	/* traps 1n */
	.org 0x50
	jr	trap_handler
	/* invalid instruction/debug */
	.org 0x60
	jr	illegal_handler

trap_handler:
	st.w	r6, -4[sp]
	mov	0, r2
	movhi	hi(trap_data), r2, r2
	movea	lo(trap_data), r2, r2
	stsr	ECR, r6
	andi	0xFFFF, r6, r6
	shl	2, r6
	add	r6, r2
	ld.w	-4[sp], r6
	ld.w	0[r2], r2
	cmp	r2, r0
	bz	no_trap_handler
	jmp	[r2]
no_trap_handler:
	reti

nmi2_handler:
	mov	0x4, r2
	addi	hi(trap_data), r2, r2
	shl	16, r2
	addi	lo(trap_data), r2, r2
	ld.w	8[r2], r2
	cmp	r2, r0
	bz	no_trap_handler
	jmp	[r2]
nmi1_handler:
	mov	0x4, r2
	addi	hi(trap_data), r2, r2
	shl	16, r2
	addi	lo(trap_data), r2, r2
	ld.w	4[r2], r2
	cmp	r2, r0
	bz	no_trap_handler
	jmp	[r2]
nmi0_handler:
	mov	0x4, r2
	addi	hi(trap_data), r2, r2
	shl	16, r2
	addi	lo(trap_data), r2, r2
	ld.w	0[r2], r2
	cmp	r2, r0
	bz	no_trap_handler
	jmp	[r2]

illegal_handler:
	mov	0x4, r2
	addi	hi(trap_data), r2, r2
	shl	16, r2
	addi	lo(trap_data), r2, r2
	ld.w	12[r2], r2
	cmp	r2, r0
	bz	no_trap_handler
	jmp	[r2]
	reti

start:
# Disable interrupts and set some pointers
	di
	movhi	hi(_stack_bot), sp, sp
	movea	lo(_stack_bot), sp, sp
	mov	0, gp
	mov	0, tp
	mov	0, ep
	mov	0, lp

# Clear the trap location datas
	movhi	hi(trap_data), r2, r2
	movea	lo(trap_data), r2, r2
	mov	0, r6
	addi	0x1000, r6, r6
	shl	4, r6
	add	4, r6
	mov	1, r7
trapclear:
	st.w	r0, 0[r2]
	add	4, r2
	sub	r7, r6
	bnl	trapclear

# Clear the GPRs
	mov	0, r2
	mov	0, r8
	mov	0, r9
	mov	0, r10
	mov	0, r11
	mov	0, r12
	mov	0, r13
	mov	0, r14
	mov	0, r15
	mov	0, r16
	mov	0, r17
	mov	0, r18
	mov	0, r19
	mov	0, r20
	mov	0, r21
	mov	0, r22
	mov	0, r23
	mov	0, r24
	mov	0, r25
	mov	0, r26
	mov	0, r27
	mov	0, r28
	mov	0, r29

# Arguments to main
# argc (r6) = 0
# argv (r7) = NULL
	mov	0, r6
	mov	0, r7

	jarl	executemain, lp
# This works because align fills with 0, which is NOP :)
	.align	4
	jr	.

	.align	4
# Small stub because we want the linking.
executemain:
	mov	0, r2
	movhi	hi(_main), r2, r2
	movea	lo(_main), r2, r2
	jmp	[r2]



